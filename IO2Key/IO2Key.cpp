// IO2Key.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"
#include "IO2Key.h"

const int MAX_LOADSTRING = 128;

// Global Variables:
HINSTANCE hInst = NULL;	// current instance
HWND hWndMain = NULL;

bool g_WaitToProbeIO = true;
bool g_ProbingIO = false;

UI* g_UI;
bool g_IsFirstRun = true;
bool g_IsWindowVisible = false;
UIEnum g_CurrentButton = UI_INVALID;
UIButton g_Buttons[NUM_UI_ELEMENTS] = {
	{ 0x5A, 0x2C, 0x00000008, NULL, _T("P1_DownLeft") },	// Z
	{ 0x51, 0x10, 0x00000001, NULL, _T("P1_UpLeft") },		// Q
	{ 0x53, 0x1F, 0x00000004, NULL, _T("P1_Center") },		// S
	{ 0x45, 0x12, 0x00000002, NULL, _T("P1_UpRight") },		// E
	{ 0x43, 0x2E, 0x00000010, NULL, _T("P1_DownRight") },	// C
	{ 0x61, 0x4F, 0x00080000, NULL, _T("P2_DownLeft") },	// KP1
	{ 0x67, 0x47, 0x00010000, NULL, _T("P2_UpLeft") },		// KP7
	{ 0x65, 0x4C, 0x00040000, NULL, _T("P2_Center") },		// KP5
	{ 0x69, 0x49, 0x00020000, NULL, _T("P2_UpRight") },		// KP9
	{ 0x63, 0x51, 0x00100000, NULL, _T("P2_DownRight") },	// KP3
	{ 0x91, 0x46, 0x00000200, NULL, _T("Test") },			// Scroll
	{ 0x91, 0x46, 0x00004000, NULL, _T("Service") },		// Scroll
	{ 0x1B, 0x01, 0x00008000, NULL, _T("Clear") },			// Escape
#ifndef NO_COIN
	{ 0x70, 0x3B, 0x00000400, NULL, _T("Coin1") },			// F1
	{ 0x7B, 0x58, 0x04000000, NULL, _T("Coin2") },			// F12
#endif
};

NOTIFYICONDATA g_TrayData = {0};
UINT g_TrayFlags[NUM_TRAY_IDS] = {
	MF_ENABLED | MF_STRING,		// TRAY_CHANGE_KEYS
	MF_ENABLED | MF_UNCHECKED,	// TRAY_START_WINDOWS
	MF_ENABLED | MF_STRING,		// TRAY_ABOUT
	MF_SEPARATOR,				// TRAY_SEPARATOR
	MF_ENABLED | MF_STRING,		// TRAY_EXIT
};
#define TRAY_COMMAND(command) (WM_APP + 1 + (command))

// Forward declarations of functions included in this code module:
ATOM				RegisterMainWindowClass(HINSTANCE hInstance);
ATOM				RegisterChangeKeyDialogClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
BOOL				CreateChangeKeyDialog();

LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	DialogProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

void RegistryError(UINT uTextID, LSTATUS lStatus);
void InitializeUI();
void DestroyUI();
void CreateTrayMenu();
void InitializeTrayData();
void DestroyTrayData();
void DrawUI(HDC hdc);
void LoadKeys();
void SaveKeys();
void EnableLoadAtStartup();
void DisableLoadAtStartup();

void BalloonText(UINT title, UINT text, BOOL modify = TRUE);
void MouseClick(int x, int y, bool down);
void KeyPressed(WPARAM wParam, LPARAM lParam, bool down);
void PressKey(int ui_button, bool down);

DWORD WINAPI IOThread(LPVOID lpParam);
void UpdateIO();

typedef void PIUIO_VOID();
typedef unsigned short PIUIO_IN();
typedef void PIUIO_OUT(unsigned short value);

struct
{
	PIUIO_VOID*	init;
	PIUIO_VOID*	deInit;
	PIUIO_IN*	inP1;
	PIUIO_IN*	inP2;
	PIUIO_OUT*	outP1;
	PIUIO_OUT*	outP2;
} g_PIUIO;

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	UNREFERENCED_PARAMETER(nCmdShow);

	HWND hWndPrevInstance = FindWindow(_T("IO2KeyMain"), NULL);
	if( hWndPrevInstance != NULL )
	{
		SendMessage(hWndPrevInstance, WM_COMMAND, MAKEWPARAM(IDM_DUPLICATE_INSTANCE, 0), 0);
		return -1;
	}

	HANDLE hIOThread = CreateThread(NULL, 0, IOThread, NULL, 0, NULL);

	// Initialize global strings
	RegisterMainWindowClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
		return FALSE;

	MSG msg;
	// Main message loop:
	while( GetMessage(&msg, NULL, 0, 0) )
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	g_ProbingIO = false;
	WaitForSingleObject(hIOThread, INFINITE);

	return (int) msg.wParam;
}

DWORD WINAPI IOThread(LPVOID lpParam)
{
	UNREFERENCED_PARAMETER(lpParam);

	HINSTANCE ioInstance = LoadLibrary(_T("usbio.dll"));
	if( !ioInstance )
		return FALSE;

	g_PIUIO.init = (PIUIO_VOID*)GetProcAddress(ioInstance, "MK6IO_Init");
	g_PIUIO.deInit = (PIUIO_VOID*)GetProcAddress(ioInstance, "MK6IO_Deinit");
	g_PIUIO.inP1 = (PIUIO_IN*)GetProcAddress(ioInstance, "MK6IO_HandleInputP1");
	g_PIUIO.inP2 = (PIUIO_IN*)GetProcAddress(ioInstance, "MK6IO_HandleInputP2");
	g_PIUIO.outP1 = (PIUIO_OUT*)GetProcAddress(ioInstance, "MK6IO_HandleOutputP1");
	g_PIUIO.outP2 = (PIUIO_OUT*)GetProcAddress(ioInstance, "MK6IO_HandleOutputP2");

	g_ProbingIO = true;
	g_PIUIO.init();

	while( g_WaitToProbeIO )
		Sleep(250);

	while( g_ProbingIO )
	{
		UpdateIO();
		Sleep(1);
	}

	g_PIUIO.deInit();
	FreeLibrary(ioInstance);
	return 0;
}

//
//  FUNCTION: RegisterMainWindowClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM RegisterMainWindowClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex = {0};
	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_IO2KEY));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= _T("IO2KeyMain");
	wcex.hIconSm		= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_IO2KEY));

	return RegisterClassEx(&wcex);
}

ATOM RegisterChangeKeyDialogClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex = {0};
	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= DialogProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_IO2KEY));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)GetStockObject(WHITE_BRUSH);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= _T("IO2KeyChangeKey");
	wcex.hIconSm		= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_IO2KEY));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	POINT dimensions = { 320, 240 };
	{
		RECT windowRect = { 0, 0, dimensions.x, dimensions.y };
		if( AdjustWindowRect(&windowRect, WS_OVERLAPPED | WS_CAPTION, FALSE) )
		{
			dimensions.x = windowRect.right - windowRect.left;
			dimensions.y = windowRect.bottom - windowRect.top;
		}
	}

	POINT position = { CW_USEDEFAULT, CW_USEDEFAULT };
	{
		RECT screenRect;
		if( SystemParametersInfo(SPI_GETWORKAREA, 0, &screenRect, 0) )
		{
			position.x = (screenRect.right - screenRect.left)/2 - dimensions.x/2;
			position.y = (screenRect.bottom - screenRect.top)/2 - dimensions.y/2;
		}
	}

	hWndMain = CreateWindow(_T("IO2KeyMain"), _T("IO2Key"), WS_OVERLAPPED | WS_CAPTION,
		position.x, position.y, dimensions.x, dimensions.y,
		NULL, NULL, hInst, NULL);

	if (!hWndMain)
	{
		return FALSE;
	}

	BringWindowToTop(hWndMain);
	SetWindowPos(hWndMain, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
	//SetWindowLong(hWndMain, GWL_EXSTYLE, GetWindowLong(hWnd, GWL_EXSTYLE) | WS_EX_NOACTIVATE);

	InitializeUI();
	InitializeTrayData();
	ShowWindow(hWndMain, SW_HIDE);

	return TRUE;
}

void RegistryError(UINT uTextID, LSTATUS lStatus)
{
	TCHAR szBuffer[MAX_LOADSTRING * 2] = _T("");

	// Read custom error
	TCHAR szError[MAX_LOADSTRING];
	LoadString(hInst, uTextID, szError, _countof(szError));
	_tcscat_s(szBuffer, _countof(szBuffer), szError);
	_tcscat_s(szBuffer, _countof(szBuffer), _T("\n\n"));

	// Read windows error
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, lStatus, 0, szError, _countof(szError), NULL);
	_tcscat_s(szBuffer, _countof(szBuffer), szError);

	MessageBox(hWndMain, szBuffer, _T("IO2Key"), MB_ICONERROR);
}

void InitializeUI()
{
	g_UI = new UI();

	Pad* pads[NUM_PADS] = {
		g_UI->fullpad->pads[PAD_LEFT],
		g_UI->fullpad->pads[PAD_RIGHT]
	};

	// Pads
	{
		RECT left = { 5, 24, 157, 176 };
		pads[PAD_LEFT]->SetRect(left);

		RECT right = { 163, 24, 315, 176 };
		pads[PAD_RIGHT]->SetRect(right);
	}

	// Panels & Sensors
	{
		RECT rect_panel[NUM_PADS][NUM_PANELS] = {
			{
				{ 15, 130, 51, 166 },
				{ 15, 34, 51, 70 },
				{ 63, 82, 99, 118 },
				{ 111, 34, 147, 70 },
				{ 111, 130, 147, 166 }
			},
			{
				{ 173, 130, 209, 166 },
				{ 173, 34, 209, 70 },
				{ 221, 82, 257, 118 },
				{ 269, 34, 305, 70 },
				{ 269, 130, 305, 166 }
			}
		};

		RECT rect_sensors[NUM_PADS][NUM_PANELS][NUM_SENSORS] = {
			{
				{
					{ 51, 130, 57, 166 },
					{ 9, 130, 15, 166 },
					{ 15, 166, 51, 172 },
					{ 15, 124, 51, 130 },
				},
				{
					{ 51, 34, 57, 70 },
					{ 9, 34, 15, 70 },
					{ 15, 70, 51, 76 },
					{ 15, 28, 51, 34 },
				},
				{
					{ 99, 82, 105, 118 },
					{ 57, 82, 63, 118 },
					{ 63, 118, 99, 124 },
					{ 63, 76, 99, 82 },
				},
				{
					{ 147, 34, 153, 70 },
					{ 105, 34, 111, 70 },
					{ 111, 70, 147, 76 },
					{ 111, 28, 147, 34 },
				},
				{
					{ 147, 130, 153, 166 },
					{ 105, 130, 111, 166 },
					{ 111, 166, 147, 172 },
					{ 111, 124, 147, 130 },
				}
			},
			{
				{
					{ 209, 130, 215, 166 },
					{ 167, 130, 173, 166 },
					{ 173, 166, 209, 172 },
					{ 173, 124, 209, 130 },
				},
				{
					{ 209, 34, 215, 70 },
					{ 167, 34, 173, 70 },
					{ 173, 70, 209, 76 },
					{ 173, 28, 209, 34 },
				},
				{
					{ 257, 82, 263, 118 },
					{ 215, 82, 221, 118 },
					{ 221, 118, 257, 124 },
					{ 221, 76, 257, 82 },
				},
				{
					{ 305, 34, 311, 70 },
					{ 263, 34, 269, 70 },
					{ 269, 70, 305, 76 },
					{ 269, 28, 305, 34 },
				},
				{
					{ 305, 130, 311, 166 },
					{ 263, 130, 269, 166 },
					{ 269, 166, 305, 172 },
					{ 269, 124, 305, 130 },
				}
			}
		};
		Panel* panel;
		for( int pd=0; pd<NUM_PADS; pd++ )
		{
			for( int pn=0; pn<NUM_PANELS; pn++ )
			{
				panel = pads[pd]->panels[pn];
				panel->SetRect(rect_panel[pd][pn]);
				g_Buttons[pd*NUM_PANELS+pn].button = panel;

				Sensor* sensors = panel->sensors;
				for( int s=0; s<NUM_SENSORS; s++ )
					sensors[s].SetRect(rect_sensors[pd][pn][s]);
			}
		}
	}

	// Buttons
	{
		RECT rect_button[NUM_BUTTONS] = {
			{ 7, 183, 42, 218 },
			{ 50, 183, 85, 218 },
			{ 97, 188, 121, 212 },
#ifndef NO_COIN
			{ 140, 182, 164, 218 },
			{ 182, 182, 206, 218 },
#endif
		};
		for( int b=0; b<NUM_BUTTONS; b++ )
		{
			g_UI->buttons[b]->SetRect(rect_button[b]);
			g_Buttons[UI_TEST+b].button = g_UI->buttons[b];
		}

		RECT clear_in = { 103, 194, 115, 206 };
		CompositeButton* clear = static_cast<CompositeButton*>(g_UI->buttons[BUTTON_CLEAR]);
		clear->SetRectIn(clear_in);

#ifndef NO_COIN
		RECT coin1_in = { 143, 185, 146, 215 };
		CompositeButton* coin1 = static_cast<CompositeButton*>(g_UI->buttons[BUTTON_COIN1]);
		coin1->SetRectIn(coin1_in);

		RECT coin2_in = { 185, 185, 188, 215 };
		CompositeButton* coin2 = static_cast<CompositeButton*>(g_UI->buttons[BUTTON_COIN2]);
		coin2->SetRectIn(coin2_in);
#endif
	}

	LoadKeys();
}

void DestroyUI()
{
	SaveKeys();
	delete g_UI;
}

void CreateTrayMenu()
{
	POINT cursorPos;
	GetCursorPos(&cursorPos);

	HMENU hMenu = CreatePopupMenu();
	if( hMenu )
	{
		for( int t = 0; t < NUM_TRAY_IDS; t++ )
		{
			TCHAR szMenuItem[MAX_LOADSTRING] = _T("");
			LoadString(hInst, IDS_TRAY_CHANGE_KEYS + t, szMenuItem, _countof(szMenuItem));
			InsertMenu(hMenu, t, g_TrayFlags[t], TRAY_COMMAND(t), szMenuItem);
		}
		SetMenuDefaultItem(hMenu, 0u, TRUE);
		SetForegroundWindow(hWndMain);
		TrackPopupMenu(hMenu, TPM_BOTTOMALIGN, cursorPos.x, cursorPos.y, 0, hWndMain, NULL);
		DestroyMenu(hMenu);
	}
}

void InitializeTrayData()
{
	g_TrayData.cbSize = sizeof(g_TrayData);
	g_TrayData.hWnd = hWndMain;
	g_TrayData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	g_TrayData.uID = 1;
	g_TrayData.uCallbackMessage = WM_APP;
	g_TrayData.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_IO2KEY));
	_tcscpy_s(g_TrayData.szTip, _countof(g_TrayData.szTip), _T("IO2Key"));

	if( g_IsFirstRun )
		BalloonText(IDS_BALLOON_TITLE, IDS_BALLOON_TEXT, FALSE);

	Shell_NotifyIcon(NIM_ADD, &g_TrayData);

	// Update check of tray option "Start with Windows"
	HKEY regKey;
	if( RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Run"), 0, KEY_READ, &regKey) != ERROR_SUCCESS )
		return;

	DWORD regType = REG_SZ;
	LONG result = RegQueryValueEx(regKey, _T("IO2Key"), NULL, &regType, NULL, NULL);
	switch( result )
	{
	case ERROR_SUCCESS:
		g_TrayFlags[TRAY_START_WINDOWS] |= MF_CHECKED;
		break;

	case ERROR_FILE_NOT_FOUND:
		g_TrayFlags[TRAY_START_WINDOWS] &= ~MF_CHECKED;
		break;
	}

	RegCloseKey(regKey);
}

void DestroyTrayData()
{
	Shell_NotifyIcon(NIM_DELETE, &g_TrayData);
}

void BalloonText( UINT title, UINT text, BOOL modify )
{
	TCHAR szBuffer[MAX_LOADSTRING];

	// Title
	LoadString(hInst, title, szBuffer, _countof(szBuffer));
	_tcscpy_s(g_TrayData.szInfoTitle, _countof(g_TrayData.szInfoTitle), szBuffer);

	// Text
	LoadString(hInst, text, szBuffer, _countof(szBuffer));
	_tcscpy_s(g_TrayData.szInfo, _countof(g_TrayData.szInfo), szBuffer);

	g_TrayData.uFlags |= NIF_INFO;
	g_TrayData.dwInfoFlags = NIIF_USER;

	if( modify )
		Shell_NotifyIcon(NIM_MODIFY, &g_TrayData);
}

void DrawUI(HDC hdc)
{
	// Draw panels and buttons
	g_UI->Draw(hdc);

	// Configure Text
	SetTextAlign(hdc, TA_CENTER);
	SetBkMode(hdc, TRANSPARENT);
	SetTextColor(hdc, COLOR_WHITE);

	// Create MS Sans Serif
	HFONT fontMSSansSerif = CreateFont(
		8, 0, 0, 0,
		FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, NONANTIALIASED_QUALITY, DEFAULT_PITCH | FF_SWISS,
		_T("MS Sans Serif")
	);
	SelectObject(hdc, fontMSSansSerif);

	TCHAR szBuffer[MAX_LOADSTRING];

	// Caption
	LoadString(hInst, IDS_MAIN_DIALOG_CAPTION, szBuffer, _countof(szBuffer));
	TextOut(hdc, 160, 6, szBuffer, _tcslen(szBuffer));

	// Test
	LoadString(hInst, IDS_MAIN_DIALOG_TEST, szBuffer, _countof(szBuffer));
	TextOut(hdc, 24, 222, szBuffer, _tcslen(szBuffer));

	// Service
	LoadString(hInst, IDS_MAIN_DIALOG_SERVICE, szBuffer, _countof(szBuffer));
	TextOut(hdc, 67, 222, szBuffer, _tcslen(szBuffer));

	// Clear
	LoadString(hInst, IDS_MAIN_DIALOG_CLEAR, szBuffer, _countof(szBuffer));
	TextOut(hdc, 109, 222, szBuffer, _tcslen(szBuffer));

#ifndef NO_COIN
	// Coin 1
	LoadString(hInst, IDS_MAIN_DIALOG_COIN1, szBuffer, _countof(szBuffer));
	TextOut(hdc, 152, 222, szBuffer, _tcslen(szBuffer));

	// Coin 2
	LoadString(hInst, IDS_MAIN_DIALOG_COIN2, szBuffer, _countof(szBuffer));
	TextOut(hdc, 194, 222, szBuffer, _tcslen(szBuffer));
#endif

	DeleteObject(fontMSSansSerif);

	// Create Fixedsys
	HFONT fontFixedsys = CreateFont(
		24, 0, 0, 0,
		FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, NONANTIALIASED_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
		_T("Fixedsys")
	);
	SelectObject(hdc, fontFixedsys);

	TCHAR szHexNumber[4];
	for( int e=0; e<NUM_UI_ELEMENTS; e++ )
	{
		const UIButton& buttonInfo = g_Buttons[e];
		RECT* rect = buttonInfo.button->GetRect();
		POINT scanPos = {
			rect->left + (rect->right - rect->left) / 2,
			rect->top + (rect->bottom - rect->top) / 2 - 16
		};

		switch( e )
		{
		case UI_P1_DOWNLEFT:
		case UI_P1_UPLEFT:
		case UI_P1_CENTER:
		case UI_P1_UPRIGHT:
		case UI_P1_DOWNRIGHT:
		case UI_P2_DOWNLEFT:
		case UI_P2_UPLEFT:
		case UI_P2_CENTER:
		case UI_P2_UPRIGHT:
		case UI_P2_DOWNRIGHT:
		case UI_CLEAR:
#ifndef NO_COIN
		case UI_COIN1:
		case UI_COIN2:
#endif
			SetTextColor(hdc, buttonInfo.button->On() ? COLOR_BLACK : COLOR_WHITE);
			break;

		case UI_SERVICE:
			SetTextColor(hdc, COLOR_BLACK);
			break;

		default:
			SetTextColor(hdc, COLOR_WHITE);
			break;
		}

		_stprintf_s(szHexNumber, _countof(szHexNumber), _T("%02X"), buttonInfo.virtualKey);
		TextOut(hdc, scanPos.x, scanPos.y, szHexNumber, _tcslen(szHexNumber));

		_stprintf_s(szHexNumber, _countof(szHexNumber), _T("%02X"), buttonInfo.scanCode);
		TextOut(hdc, scanPos.x, scanPos.y + 16, szHexNumber, _tcslen(szHexNumber));
	}

	DeleteObject(fontFixedsys);
}

void LoadKeys()
{
	HKEY regKey;
	if( RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Aldo_MX\\IO2Key"), 0, KEY_READ, &regKey) != ERROR_SUCCESS )
	{
		// No keys to read, can probe IO now
		g_WaitToProbeIO = false;
		return;
	}

	// Assume first run if no reg key exists
	g_IsFirstRun = false;

	for( int e = 0; e < NUM_UI_ELEMENTS; e++ )
	{
		UIButton& buttonInfo = g_Buttons[e];
		DWORD regValue = 0, regType = REG_DWORD, cbData = sizeof(DWORD);
		if( RegQueryValueEx(regKey, buttonInfo.name, NULL, &regType, (LPBYTE)(&regValue), &cbData) != ERROR_SUCCESS )
			continue;

		buttonInfo.virtualKey = regValue & 0x0000ffff;
		buttonInfo.scanCode = (regValue & 0xffff0000) >> 16;
	}

	// Keys are read, can probe IO now
	g_WaitToProbeIO = false;

	RegCloseKey(regKey);
}

void SaveKeys()
{
	HKEY regKey;
	LSTATUS lStatus = RegCreateKeyEx(HKEY_CURRENT_USER, _T("Software\\Aldo_MX\\IO2Key"), 0, NULL, 0, KEY_WRITE, NULL, &regKey, NULL);
	if( lStatus != ERROR_SUCCESS )
	{
		RegistryError(IDS_ERROR_SAVE_REGKEY_KEYS, lStatus);
		return;
	}

	for( int e = 0; e < NUM_UI_ELEMENTS; e++ )
	{
		const UIButton& buttonInfo = g_Buttons[e];
		DWORD regValue = buttonInfo.virtualKey | (buttonInfo.scanCode << 16);
		RegSetValueEx(regKey, buttonInfo.name, 0, REG_DWORD, (const BYTE*)(&regValue), sizeof(DWORD));
	}

	RegCloseKey(regKey);
}

void EnableLoadAtStartup()
{
	HKEY regKey;
	if( RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Run"), 0, KEY_WRITE, &regKey) != ERROR_SUCCESS )
		return;

	DWORD dwCharsFileName;
	LSTATUS lStatus;

	TCHAR szPath[MAX_PATH];
	if( (dwCharsFileName = GetModuleFileName(NULL, szPath, _countof(szPath))) == 0 )
		RegistryError(IDS_ERROR_GET_FILENAME, GetLastError());
	else if( dwCharsFileName == _countof(szPath) )
		RegistryError(IDS_ERROR_GET_FILENAME, ERROR_INSUFFICIENT_BUFFER);
	else if( (lStatus = RegSetValueEx(regKey, _T("IO2Key"), 0, REG_SZ, (const BYTE*)szPath, _tcslen(szPath) * sizeof(TCHAR))) != ERROR_SUCCESS )
		RegistryError(IDS_ERROR_SAVE_REGKEY_STARTUP, lStatus);
	else
		g_TrayFlags[TRAY_START_WINDOWS] |= MF_CHECKED;

	RegCloseKey(regKey);
}

void DisableLoadAtStartup()
{
	HKEY regKey;
	if( RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Run"), 0, KEY_WRITE, &regKey) != ERROR_SUCCESS )
		return;

	LSTATUS lStatus;
	if( (lStatus = RegDeleteValue(regKey, _T("IO2Key"))) != ERROR_SUCCESS )
		RegistryError(IDS_ERROR_DELETE_REGKEY_STARTUP, lStatus);
	else
		g_TrayFlags[TRAY_START_WINDOWS] &= ~MF_CHECKED;

	RegCloseKey(regKey);
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_APP:
		switch(lParam)
		{
		case WM_LBUTTONDBLCLK:
			ShowWindow(hWnd, SW_RESTORE);
			UpdateWindow(hWnd);
			break;

		case WM_RBUTTONUP:
		case WM_CONTEXTMENU:
			CreateTrayMenu();
			break;
		}
		break;

	case WM_CREATE: {
		RegisterChangeKeyDialogClass(hInst);
		TCHAR szOkButton[MAX_LOADSTRING];
		LoadString(hInst, IDS_MAIN_DIALOG_OK, szOkButton, _countof(szOkButton));
		CreateWindow(
			_T("button"), szOkButton, WS_CHILD | WS_VISIBLE | BS_FLAT,
			224, 196, 86, 24,
			hWnd, (HMENU)IDM_MAIN_DIALOG_OK, NULL, NULL);
		break; }

	case WM_COMMAND: {
		int wmId    = LOWORD(wParam);
		int wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_MAIN_DIALOG_OK:
			SaveKeys();
			ShowWindow(hWnd, SW_HIDE);
			BalloonText(IDS_BALLOON_TITLE_MINIMIZED, IDS_BALLOON_TEXT_MINIMIZED);
			break;

		case IDM_DUPLICATE_INSTANCE:
			BalloonText(IDS_BALLOON_TITLE_DUP_INST, IDS_BALLOON_TEXT_DUP_INST);
			break;

		case TRAY_COMMAND(TRAY_CHANGE_KEYS):
			ShowWindow(hWnd, SW_RESTORE);
			UpdateWindow(hWnd);
			break;

		case TRAY_COMMAND(TRAY_START_WINDOWS):
			if( g_TrayFlags[TRAY_START_WINDOWS] & MF_CHECKED )
				DisableLoadAtStartup();
			else
				EnableLoadAtStartup();
			break;

		case TRAY_COMMAND(TRAY_ABOUT):
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;

		case TRAY_COMMAND(TRAY_EXIT):
			DestroyWindow(hWnd);
			break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break; }

	case WM_SHOWWINDOW:
		g_IsWindowVisible = wParam == TRUE;
		if( g_IsWindowVisible )
			InvalidateRect(hWndMain, NULL, true);
		break;

	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
		MouseClick(LOWORD(lParam), HIWORD(lParam), message == WM_LBUTTONDOWN);
		return 0;

	case WM_PAINT: {
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		DrawUI(hdc);
		EndPaint(hWnd, &ps);
		return 0; }

	case WM_DESTROY:
		g_IsWindowVisible = false;
		DestroyUI();
		DestroyTrayData();
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc( hWnd, message, wParam, lParam );
}

LRESULT CALLBACK DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_KEYDOWN:
	case WM_KEYUP: {
		bool isDown = message == WM_KEYDOWN;
		KeyPressed(wParam, lParam, isDown);
		if( !isDown )
		{
			g_CurrentButton = UI_INVALID;
			DestroyWindow(hWnd);
		}
		return 0; }

	case WM_PAINT: {
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		// Configure Text
		SetTextAlign(hdc, TA_CENTER);
		SetBkMode(hdc, TRANSPARENT);
		SetTextColor(hdc, COLOR_BLACK);

		// Create MS Sans Serif
		HFONT fontMSSansSerif = CreateFont(
			0, 0, 0, 0,
			FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET,
			OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
			_T("MS Sans Serif")
		);
		SelectObject(hdc, fontMSSansSerif);

		// Dialog Text
		TCHAR szText[MAX_LOADSTRING];
		LoadString(hInst, IDS_CHANGE_KEY_DIALOG_TEXT, szText, _countof(szText));
		TextOut(hdc, 80, 8, szText, _tcslen(szText));

		DeleteObject(fontMSSansSerif);
		EndPaint(hWnd, &ps);
		return 0; }
	}

	return DefWindowProc( hWnd, message, wParam, lParam );
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

BOOL CreateChangeKeyDialog()
{
	POINT dimensions = { 160, 32 };
	{
		RECT windowRect = { 0, 0, dimensions.x, dimensions.y };
		if( AdjustWindowRect(&windowRect, WS_OVERLAPPED | WS_CAPTION, FALSE) )
		{
			dimensions.x = windowRect.right - windowRect.left;
			dimensions.y = windowRect.bottom - windowRect.top;
		}
	}

	POINT position = { CW_USEDEFAULT, CW_USEDEFAULT };
	{
		RECT screenRect;
		if( SystemParametersInfo(SPI_GETWORKAREA, 0, &screenRect, 0) )
		{
			position.x = (screenRect.right - screenRect.left)/2 - dimensions.x/2;
			position.y = (screenRect.bottom - screenRect.top)/2 - dimensions.y/2;
		}
	}

	TCHAR szTitle[MAX_LOADSTRING];
	LoadString(hInst, IDS_CHANGE_KEY_DIALOG_TITLE, szTitle, _countof(szTitle));

	HWND hWnd = CreateWindow(_T("IO2KeyChangeKey"), szTitle, WS_OVERLAPPED | WS_CAPTION,
		position.x, position.y, dimensions.x, dimensions.y,
		hWndMain, NULL, hInst, NULL);

	if( !hWnd )
		return FALSE;

	BringWindowToTop(hWnd);
	SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);

	ShowWindow(hWnd, SW_SHOWDEFAULT);
	UpdateWindow(hWnd);

	return TRUE;
}

void MouseClick( int x, int y, bool down )
{
	for( int e=0; e<NUM_UI_ELEMENTS; e++ )
	{
		if( g_Buttons[e].button->InRange(x, y) )
		{
			if( down )
				g_Buttons[e].button->Press();
			else
			{
				g_Buttons[e].button->Release();

				g_CurrentButton = (UIEnum)e;
				CreateChangeKeyDialog();
			}

			InvalidateRect(hWndMain, g_Buttons[e].button->GetRect(), true);
			break;
		}
	}
}

void KeyPressed( WPARAM wParam, LPARAM lParam, bool down )
{
	if( g_CurrentButton == UI_INVALID )
		return;

	UIButton& buttonInfo = g_Buttons[g_CurrentButton];
	buttonInfo.virtualKey = wParam;
	buttonInfo.scanCode = (lParam & 0xff0000) >> 16;

	if( down )
		buttonInfo.button->Press();
	else
		buttonInfo.button->Release();

	InvalidateRect(hWndMain, buttonInfo.button->GetRect(), true);
}

void PressKey( int ui_button, bool down )
{
	UIButton& buttonInfo = g_Buttons[ui_button];

	INPUT input;
	input.type = INPUT_KEYBOARD;
	input.ki.wScan = buttonInfo.scanCode;
	input.ki.time = GetTickCount();
	input.ki.dwExtraInfo = 0;
	input.ki.wVk = buttonInfo.virtualKey;
	input.ki.dwFlags = 0;

	if( !down )
		input.ki.dwFlags |= KEYEVENTF_KEYUP;

	if( buttonInfo.scanCode > 0 )
		input.ki.dwFlags |= KEYEVENTF_SCANCODE;

	SendInput(1, &input, sizeof(input));
}

void UpdateIO()
{
	ElementInfo elementInfo[NUM_UI_ELEMENTS];

	for( unsigned short s = 0; s < NUM_SENSORS; s++ )
	{
		UINT32 ioIn = ~0;
		g_PIUIO.outP1(s);
		g_PIUIO.outP2(s);
		ioIn &= g_PIUIO.inP1() | g_PIUIO.inP2() << 16;

		for( int e = 0; e < NUM_UI_ELEMENTS; e++ )
		{
			UIButton& buttonInfo = g_Buttons[e];
			bool isElementOn = !(ioIn & buttonInfo.ioBits);
			elementInfo[e].anySensorOn |= isElementOn;

			if( buttonInfo.button->On() == isElementOn )
				continue;

			elementInfo[e].needsUpdate = true;

			switch( e )
			{
				case UI_P1_DOWNLEFT:
				case UI_P1_UPLEFT:
				case UI_P1_CENTER:
				case UI_P1_UPRIGHT:
				case UI_P1_DOWNRIGHT:
				case UI_P2_DOWNLEFT:
				case UI_P2_UPLEFT:
				case UI_P2_CENTER:
				case UI_P2_UPRIGHT:
				case UI_P2_DOWNRIGHT:
					break;

				default:
					continue;
			}

			Sensor& sensor = ((Panel*)buttonInfo.button)->sensors[s];
			if( sensor.On() == isElementOn )
				continue;

			if( isElementOn )
				sensor.Press();
			else
				sensor.Release();

			if( g_IsWindowVisible )
				InvalidateRect(hWndMain, sensor.GetRect(), true);
		}
	}

	for( int e = 0; e < NUM_UI_ELEMENTS; e++ )
	{
		if( !elementInfo[e].needsUpdate )
			continue;

		UIButton& buttonInfo = g_Buttons[e];

		if( buttonInfo.button->On() == elementInfo[e].anySensorOn )
			continue;

		if( elementInfo[e].anySensorOn )
		{
			buttonInfo.button->Press();
			PressKey(e, true);
		}
		else
		{
			buttonInfo.button->Release();
			PressKey(e, false);
		}
		
		if( g_IsWindowVisible )
			InvalidateRect(hWndMain, buttonInfo.button->GetRect(), true);
	}
}
