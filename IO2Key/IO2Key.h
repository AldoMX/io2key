#pragma once

#include "stdafx.h"

const COLORREF COLOR_WHITE = RGB(255, 255, 255);
const COLORREF COLOR_BLACK = RGB(0, 0, 0);

const COLORREF COLOR_SENSOR_OFF = RGB(0, 0, 0);
const COLORREF COLOR_SENSOR_ON = RGB(255, 242, 0);

const COLORREF COLOR_PANEL_RED = RGB(237, 28, 36);
const COLORREF COLOR_PANEL_BLUE = RGB(0, 162, 232);
const COLORREF COLOR_PANEL_YELLOW = RGB(255, 201, 14);
const COLORREF COLOR_PANEL_OFF = RGB(102, 102, 127);

const COLORREF COLOR_BUTTON_TEST_ON = RGB(136, 0, 21);
const COLORREF COLOR_BUTTON_TEST_OFF = RGB(237, 28, 36);
const COLORREF COLOR_BUTTON_SERVICE_ON = RGB(195, 195, 195);
const COLORREF COLOR_BUTTON_SERVICE_OFF = RGB(255, 255, 255);
const COLORREF COLOR_BUTTON_CLEAR_ON = RGB(192, 192, 192);
const COLORREF COLOR_BUTTON_CLEAR_OFF = RGB(127, 127, 127);
#ifndef NO_COIN
const COLORREF COLOR_BUTTON_COIN_ON = RGB(181, 230, 29);
const COLORREF COLOR_BUTTON_COIN_OFF = RGB(34, 177, 76);
#endif

enum SensorEnum
{
	SENSOR_RIGHT,
	SENSOR_LEFT,
	SENSOR_DOWN,
	SENSOR_UP,
	NUM_SENSORS
};

enum PanelEnum
{
	PANEL_DOWNLEFT,
	PANEL_UPLEFT,
	PANEL_CENTER,
	PANEL_UPRIGHT,
	PANEL_DOWNRIGHT,
	NUM_PANELS
};

enum PadEnum
{
	PAD_LEFT,
	PAD_RIGHT,
	NUM_PADS
};

enum ButtonEnum
{
	BUTTON_TEST,
	BUTTON_SERVICE,
	BUTTON_CLEAR,
#ifndef NO_COIN
	BUTTON_COIN1,
	BUTTON_COIN2,
#endif
	NUM_BUTTONS
};

enum UIEnum
{
	UI_P1_DOWNLEFT,
	UI_P1_UPLEFT,
	UI_P1_CENTER,
	UI_P1_UPRIGHT,
	UI_P1_DOWNRIGHT,
	UI_P2_DOWNLEFT,
	UI_P2_UPLEFT,
	UI_P2_CENTER,
	UI_P2_UPRIGHT,
	UI_P2_DOWNRIGHT,
	UI_TEST,
	UI_SERVICE,
	UI_CLEAR,
#ifndef NO_COIN
	UI_COIN1,
	UI_COIN2,
#endif
	NUM_UI_ELEMENTS,
	UI_INVALID
};

class Button
{
	bool on;

	RECT rect;
	HBRUSH brush_on;
	HBRUSH brush_off;
	inline void DeleteHBrush() {
		if( brush_on ) DeleteObject(brush_on);
		if( brush_off ) DeleteObject(brush_off);
	}

public:
	Button() {
		Release();
		brush_on = NULL;
		brush_off = NULL;
	};
	~Button() { DeleteHBrush(); }

	static void SetRect( RECT& rect1, RECT& rect2 )
	{
		rect1.left = rect2.left;
		rect1.top = rect2.top;
		rect1.right = rect2.right;
		rect1.bottom = rect2.bottom;
	}
	static inline void DrawRect( HDC hdc, RECT& rect ) {
		Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
	}
	static inline void DrawCircle( HDC hdc, RECT& rect ) {
		Ellipse(hdc, rect.left, rect.top, rect.right, rect.bottom);
	}
	static inline bool InRange( int x, int y, RECT& rect )
	{
		return x >= rect.left && x <= rect.right &&
			y >= rect.top && y <= rect.bottom;
	}

	inline bool On() { return on; };
	inline void Press() { on = true; };
	inline void Release() { on = false; };

	void SetColor( COLORREF on, COLORREF off ) {
		DeleteHBrush();
		brush_on = CreateSolidBrush(on);
		brush_off = CreateSolidBrush(off);
	}
	inline void SetBrush( HDC hdc ) { SelectObject(hdc, on ? brush_on : brush_off); }
	
	inline RECT* GetRect() { return &rect; }
	inline void SetRect( RECT& rect_ ) { SetRect(rect, rect_); }
	inline void DrawRect( HDC hdc ) { DrawRect(hdc, rect); }
	inline void DrawCircle( HDC hdc ) { DrawCircle(hdc, rect); }
	inline bool InRange( int x, int y ) { return InRange( x, y, rect ); }

	virtual void Draw( HDC hdc ) = 0;
};

class CompositeButton: public Button
{
	RECT rect_in;
	HBRUSH brush_in_on;
	HBRUSH brush_in_off;
	inline void DeleteHBrushIn() {
		if( brush_in_on ) DeleteObject(brush_in_on);
		if( brush_in_off ) DeleteObject(brush_in_off);
	}

public:
	CompositeButton() : Button() {
		brush_in_on = NULL;
		brush_in_off = NULL;
	}
	~CompositeButton() {
		DeleteHBrushIn();
		Button::~Button();
	}

	void SetColorIn( COLORREF on, COLORREF off ) {
		DeleteHBrushIn();
		brush_in_on = CreateSolidBrush(on);
		brush_in_off = CreateSolidBrush(off);
	}
	inline void SetBrushIn( HDC hdc ) { SelectObject(hdc, On() ? brush_in_on : brush_in_off); }

	inline void SetRectIn( RECT& rect_ ) { SetRect(rect_in, rect_); }
	inline void DrawRectIn( HDC hdc ) { DrawRect(hdc, rect_in); }
	inline void DrawCircleIn( HDC hdc ) { DrawCircle(hdc, rect_in); }
};

struct Sensor: Button
{
	Sensor() : Button() { SetColor(COLOR_SENSOR_ON, COLOR_SENSOR_OFF); }
	void Draw( HDC hdc ) { SetBrush(hdc); DrawRect(hdc); }
};

struct Panel: Button
{
	Panel( COLORREF on, COLORREF off ): Button() { SetColor(on, off); }
	Sensor sensors[NUM_SENSORS];
	void Draw( HDC hdc ) {
		SetBrush(hdc); DrawRect(hdc);
		for( int s=0; s<NUM_SENSORS; s++ )
			sensors[s].Draw(hdc);
	}
};

struct Pad: Button
{
	Pad() : Button() {
		for( int p=0; p<NUM_PANELS; p++ )
			panels[p] = PanelFactory(p);
		SetColor(COLOR_PANEL_OFF, COLOR_PANEL_OFF);
	}
	~Pad() {
		for( int p=0; p<NUM_PANELS; p++ )
			delete panels[p];
	}

	Panel* panels[NUM_PANELS];
	void Draw( HDC hdc ) {
		SetBrush(hdc); DrawRect(hdc);
		for( int p=0; p<NUM_PANELS; p++ )
			panels[p]->Draw(hdc);
	}

	Panel* PanelFactory( int id )
	{
		switch(id)
		{
		case PANEL_DOWNLEFT:
		case PANEL_DOWNRIGHT:
			return new Panel(COLOR_PANEL_BLUE, COLOR_PANEL_OFF);

		case PANEL_UPLEFT:
		case PANEL_UPRIGHT:
			return new Panel(COLOR_PANEL_RED, COLOR_PANEL_OFF);

		case PANEL_CENTER:
			return new Panel(COLOR_PANEL_YELLOW, COLOR_PANEL_OFF);

		default:
			assert(0);
			return NULL;
		}
	}
};

struct FullPad
{
	FullPad() {
		for( int p=0; p<NUM_PADS; p++ )
			pads[p] = new Pad();
	}
	~FullPad() {
		for( int p=0; p<NUM_PADS; p++ )
			delete pads[p];
	}

	Pad* pads[NUM_PADS];
	void Draw( HDC hdc ) {
		for( int p=0; p<NUM_PADS; p++ )
			pads[p]->Draw(hdc);
	}
};

struct CircleButton: Button
{
	CircleButton( COLORREF on, COLORREF off ): Button() {
		SetColor(on, off);
	}
	void Draw( HDC hdc ) { SetBrush(hdc); DrawCircle(hdc); }
};

struct ClearButton: CompositeButton
{
	ClearButton(): CompositeButton() {
		SetColor(COLOR_BUTTON_CLEAR_ON, COLOR_BUTTON_CLEAR_OFF);
		SetColorIn(COLOR_SENSOR_OFF, COLOR_SENSOR_OFF);
	}

	void Draw( HDC hdc ) {
		SetBrush(hdc); DrawRect(hdc);
		SetBrushIn(hdc); DrawCircleIn(hdc);
	}
};

#ifndef NO_COIN
struct CoinButton: CompositeButton
{
	CoinButton(): CompositeButton() {
		SetColor(COLOR_BUTTON_COIN_ON, COLOR_BUTTON_COIN_OFF);
		SetColorIn(COLOR_SENSOR_OFF, COLOR_SENSOR_OFF);
	}
	void Draw( HDC hdc ) {
		SetBrush(hdc); DrawRect(hdc);
		SetBrushIn(hdc); DrawRectIn(hdc);
	}
};
#endif

struct UI
{
	UI() {
		fullpad = new FullPad();
		for( int b=0; b<NUM_BUTTONS; b++ )
			buttons[b] = ButtonFactory(b);
	}
	~UI() {
		delete fullpad;
		for( int b=0; b<NUM_BUTTONS; b++ )
			delete buttons[b];
	}

	FullPad* fullpad;
	Button* buttons[NUM_BUTTONS];

	void Draw( HDC hdc ) {
		fullpad->Draw(hdc);
		for( int b=0; b<NUM_BUTTONS; b++ )
			buttons[b]->Draw(hdc);
	}

	Button* ButtonFactory( int id )
	{
		switch(id)
		{
		case BUTTON_TEST:
			return new CircleButton(COLOR_BUTTON_TEST_ON, COLOR_BUTTON_TEST_OFF);

		case BUTTON_SERVICE:
			return new CircleButton(COLOR_BUTTON_SERVICE_ON, COLOR_BUTTON_SERVICE_OFF);

		case BUTTON_CLEAR:
			return new ClearButton();

#ifndef NO_COIN
		case BUTTON_COIN1:
		case BUTTON_COIN2:
			return new CoinButton();
#endif

		default:
			assert(0);
			return NULL;
		}
	}
};

struct UIButton
{
	unsigned short	virtualKey;
	unsigned short	scanCode;
	unsigned int	ioBits;
	Button			*button;
	TCHAR			*name;
};

struct ElementInfo {
	bool anySensorOn = false;
	bool needsUpdate = false;
};

enum TrayID
{
	TRAY_CHANGE_KEYS,
	TRAY_START_WINDOWS,
	TRAY_ABOUT,
	TRAY_SEPARATOR,
	TRAY_EXIT,
	NUM_TRAY_IDS
};
