// Used by IO2Key.rc
//

#define IDD_ABOUTBOX                    101

#define IDM_MAIN_DIALOG_OK              111
#define IDM_DUPLICATE_INSTANCE          112

#define IDI_IO2KEY                      121

#define IDS_MAIN_DIALOG_OK              131
#define IDS_MAIN_DIALOG_CAPTION         132
#define IDS_MAIN_DIALOG_TEST            133
#define IDS_MAIN_DIALOG_SERVICE         134
#define IDS_MAIN_DIALOG_CLEAR           135
#define IDS_MAIN_DIALOG_COIN1           136
#define IDS_MAIN_DIALOG_COIN2           137
#define IDS_CHANGE_KEY_DIALOG_TITLE     138
#define IDS_CHANGE_KEY_DIALOG_TEXT      139
#define IDS_BALLOON_TITLE               140
#define IDS_BALLOON_TEXT                141
#define IDS_BALLOON_TITLE_MINIMIZED     142
#define IDS_BALLOON_TEXT_MINIMIZED      143
#define IDS_BALLOON_TITLE_DUP_INST      144
#define IDS_BALLOON_TEXT_DUP_INST       145
#define IDS_ERROR_GET_FILENAME          146
#define IDS_ERROR_SAVE_REGKEY_KEYS      147
#define IDS_ERROR_SAVE_REGKEY_STARTUP   148
#define IDS_ERROR_DELETE_REGKEY_STARTUP 149
#define IDS_TRAY_CHANGE_KEYS            150
#define IDS_TRAY_START_WINDOWS          151
#define IDS_TRAY_ABOUT                  152
#define IDS_TRAY_SEPARATOR              153
#define IDS_TRAY_EXIT                   154

#define IDC_STATIC                      -1
